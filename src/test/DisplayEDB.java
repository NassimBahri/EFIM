package test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DisplayEDB extends JFrame {

	private static final long serialVersionUID = 1L;

	public DisplayEDB(String filename) {
		String elementsLine, massLine;
		String[] elementsList, massList;
		String cl;
		String[] columns = new String[] { "ID","A", "B", "C", "CL" };
		Object[][] rowData = {};
		DefaultTableModel listTableModel = new DefaultTableModel(rowData, columns);
		try {
			InputStream inputStream = new FileInputStream("datasets/"+filename);
			InputStreamReader reader = new InputStreamReader(inputStream);
			@SuppressWarnings("resource")
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber=1;
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(";");
				massList = massLine.split(";");
				cl = massList[massList.length - 1];
				ArrayList<String> data = new ArrayList<String>();
				data.add(lineNumber+"");
				for(int i=0;i<elementsList.length;i++) {
					String tmp="";
					String [] feList = elementsList[i].split(" "), feMass = massList[i].split(" ");
					for(int j=0;j<feList.length;j++) {
						tmp+="("+feList[j]+") "+feMass[j]+" / ";
					}
					data.add(tmp);
				}
				data.add("["+cl.replace(" ", ",")+"]");
				listTableModel.addRow(data.toArray());
				lineNumber++;
			}
			// create table with data
			JTable table = new JTable(listTableModel);
			table.setRowHeight(table.getRowHeight() * 2);

			// add the table to the frame
			this.add(new JScrollPane(table));

			this.setTitle("Evidential Database "+filename);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.pack();
			this.setVisible(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new DisplayEDB("data-10000.data");
	}

}
