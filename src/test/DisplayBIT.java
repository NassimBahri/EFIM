package test;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import hpss.Bit;
import hpss.Node;

public class DisplayBIT extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTree arbre;

	/**
	 * constructor of the current class
	 * 
	 * @param node
	 */
	public DisplayBIT(Node node) {
		this.setSize(800, 700);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("BitTree sctructure");
		DefaultMutableTreeNode racine = new DefaultMutableTreeNode("∅");
		buildTree(racine, node);
		arbre = new JTree(racine);
		expandAllNodes(arbre, 0, arbre.getRowCount());
		this.getContentPane().add(new JScrollPane(arbre));
		this.setVisible(true);
	}

	/**
	 * recursive method for tree building
	 * 
	 * @param racine
	 * @param node
	 */
	private void buildTree(DefaultMutableTreeNode racine, Node node) {
		// Nous allons ajouter des branches et des feuilles à notre racine
		for (Node child : node.children) {
			DefaultMutableTreeNode rep = new DefaultMutableTreeNode(child.focalElement + "(" + child.mass/4 + ")");
			if (child.children.size() > 0) {
				buildTree(rep, child);
			}
			racine.add(rep);
		}

	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Bit bTree = new Bit("datasets/article.data");
		DisplayBIT fen = new DisplayBIT(bTree.root);
	}
	
	private void expandAllNodes(JTree tree, int startingIndex, int rowCount){
	    for(int i=startingIndex;i<rowCount;++i){
	        tree.expandRow(i);
	    }

	    if(tree.getRowCount()!=rowCount){
	        expandAllNodes(tree, rowCount, tree.getRowCount());
	    }
	}

}

