package test;


import java.util.ArrayList;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import fimed.Cell;
import fimed.RidLists;

public class DisplayRidLists extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DisplayRidLists(String filename) {
		RidLists rid = new RidLists(filename);
		String[] columns = new String[] { "Item","RID" };
		Object[][] rowData = {};
		DefaultTableModel listTableModel = new DefaultTableModel(rowData, columns);
		for(Entry<String, Cell> entry:rid.records.entrySet()) {
			ArrayList<String> data = new ArrayList<String>();
			data.add(entry.getKey());
			data.add(entry.getValue().toString());
			listTableModel.addRow(data.toArray());
		}
		// create table with data
		JTable table = new JTable(listTableModel);
		table.setRowHeight(table.getRowHeight() * 2);
		// add the table to the frame
		this.add(new JScrollPane(table));

		this.setTitle("Evidential Database "+filename);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

	public static void main(String[] args) {
		new DisplayRidLists("datasets/testapp.data");
	}
	
}
