package test;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import edma.EDMA;
import edma.EDMA.SupportBased;
import fimed.FIMED;
import pruning.DecrementalFIM;
import utils.FrequentItemSet;

public class EvidentialFIM {

	public static void main(String[] args) {
		long time = 0;
		switch (args[0]) {
		case "fimed":
			time = fimedTest(args[1], Double.parseDouble(args[2]));
			break;
		case "edma":
			time = edmaTest(args[1], Double.parseDouble(args[2]));
			break;
		case "dec":
			time = decTest(args[1], Double.parseDouble(args[2]));
			break;
		}
		System.out.println("Time spent : " + time);
	}

	/**
	 * Test FIMED Algorithm
	 * 
	 * @param file
	 *            data set
	 * @param minsup
	 *            threshold of support
	 * @return Execution time
	 */
	private static long fimedTest(String file, double minsup) {
		long startTime = System.currentTimeMillis(); // start time
		FIMED fimed = new FIMED(file);
		long stopTime = System.currentTimeMillis(); // end time
		System.out.println("***** RIDLists generated ("+(stopTime - startTime)+") *****");
		startTime = System.currentTimeMillis(); // start time
		Map<Integer, ArrayList<utils.FrequentItemSet>> frequentItemsets = fimed.ComputeFrequentItemsets(minsup);
		stopTime = System.currentTimeMillis(); // end time
		int nb = 0;
		for (Entry<Integer, ArrayList<utils.FrequentItemSet>> item : frequentItemsets.entrySet()) {
			nb += item.getValue().size();
			for(FrequentItemSet fm:item.getValue()){
				System.out.println(fm.itemset+" : "+fm.support);
			}
		}
		System.out.println("Number of frequent itemsets = " + nb);
		System.out.println("=====================");
		return stopTime - startTime; // execution time
	}

	/**
	 * Test EDMA Algorithm
	 * 
	 * @param file
	 *            data set
	 * @param minsup
	 *            threshold of support
	 * @return Execution time
	 */
	private static long edmaTest(String file, double minsup) {
		long startTime = System.currentTimeMillis(); // start time
		EDMA edma = new EDMA(file, SupportBased.bel);
		long stopTime = System.currentTimeMillis(); // end time
		System.out.println("***** Pr Table generated ("+(stopTime - startTime)+") *****");
		startTime = System.currentTimeMillis(); // start time
		Map<Integer, ArrayList<utils.FrequentItemSet>> frequentItemsets = edma.ComputeFrequentItemsets(minsup);
		stopTime = System.currentTimeMillis(); // end time
		int nb = 0;
		for (Entry<Integer, ArrayList<utils.FrequentItemSet>> item : frequentItemsets.entrySet()) {
			nb += item.getValue().size();
			for(FrequentItemSet fm:item.getValue()){
				System.out.println(fm.itemset+" : "+fm.support);
			}
		}
		System.out.println("Number of frequent itemsets = " + nb);
		System.out.println("=====================");
		return stopTime - startTime; // execution time
	}

	/**
	 * Test Decremental pruning Algorithm
	 * 
	 * @param file
	 *            data set
	 * @param minsup
	 *            threshold of support
	 * @return Execution time
	 */
	private static long decTest(String file, double minsup) {
		DecrementalFIM dec = new DecrementalFIM(file);
		System.out.println("***** RIDLists generated *****");
		long startTime = System.currentTimeMillis(); // start time
		Map<Integer, ArrayList<utils.FrequentItemSet>> frequentItemsets = dec.ComputeFrequentItemsets(minsup);
		long stopTime = System.currentTimeMillis(); // end time
		int nb = 0;
		for (Entry<Integer, ArrayList<utils.FrequentItemSet>> item : frequentItemsets.entrySet()) {
			nb += item.getValue().size();
			for(FrequentItemSet fm:item.getValue()){
				System.out.println(fm.itemset+" : "+fm.support);
			}
		}
		System.out.println("Number of frequent itemsets = " + nb);
		System.out.println("=====================");
		return stopTime - startTime; // execution time
	}

}
