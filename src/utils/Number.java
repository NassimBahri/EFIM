package utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Number {

	public static double round(double number) {
		return (double) Math.round(number * Math.pow(10, 15)) / Math.pow(10, 15);
	}

	public static void format() {
		DecimalFormat df = new DecimalFormat("#.#####");
		df.setRoundingMode(RoundingMode.HALF_UP);
		df.format(0.912385); // Le nombre sera arrondi à 0,91239.

	}

}
