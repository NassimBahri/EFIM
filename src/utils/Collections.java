package utils;

import java.util.ArrayList;

/**
 * <strong>Collections class</strong> consists exclusively of static methods
 * that operate on or return collections.
 * 
 * 
 * @author Nassim BAHRI
 * @version 1.0
 *
 */
public class Collections {

	/**
	 * Verify if list2 is a subset of list1
	 * 
	 * @param list2
	 *            Specifies the list to search
	 * @param list1
	 *            Specifies where to begin the search
	 * @return True if list2 is included in list1
	 */
	public static boolean isSubsetOf(String[] list2, String[] list1) {
		boolean exist;
		int i;
		for (String elem2 : list2) {
			exist = false;
			i = 0;
			while (i < list1.length && !exist) {
				if (elem2.trim().equals(list1[i].trim())) {
					exist = true;
				}
				i++;
			}
			if (!exist) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Calculate the number of shared items between two lists
	 * 
	 * @param list1
	 *            List 1
	 * @param list2
	 *            List 2
	 * @return Number of shared items
	 */
	public static int calculateIntersection(String[] list1, String[] list2) {
		int nb = 0;
		for (String element1 : list1) {
			for (String element2 : list2) {
				if (element1.equals(element2)) {
					nb++;
				}
			}
		}
		return nb++;
	}

	/**
	 * Generate power set from list of items
	 * 
	 * @param set
	 *            List of items
	 * @return List of generated items
	 */
	public static ArrayList<String> generatePowerSet(ArrayList<String> set) {
		int n = set.size();
		ArrayList<String> powerSet = new ArrayList<String>();
		for (int i = 1; i < (1 << n); i++) {
			String item = "";
			for (int j = 0; j < n; j++) {
				if ((i & (1 << j)) > 0) {
					item += set.get(j) + ";";
				}
			}
			item = item.substring(0, item.length() - 1);
			powerSet.add(item);
		}
		return powerSet;
	}

	/**
	 * Generate power set from list of items
	 * 
	 * @param set
	 *            List of items
	 * @param separator
	 *            Separator between items
	 * @return List of generated items
	 */
	public static ArrayList<String> generatePowerSet(ArrayList<String> set, String separator) {
		int n = set.size();
		ArrayList<String> powerSet = new ArrayList<String>();
		for (int i = 1; i < (1 << n); i++) {
			String item = "";
			for (int j = 0; j < n; j++) {
				if ((i & (1 << j)) > 0) {
					item += set.get(j) + separator;
				}
			}
			item = item.substring(0, item.length() - 1);
			powerSet.add(item);
		}
		return powerSet;
	}
}
