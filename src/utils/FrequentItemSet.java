package utils;

public class FrequentItemSet {

	public String itemset;
	public double support;

	public FrequentItemSet(String itemset, double support) {
		this.itemset = itemset;
		this.support = support;
	}

}
