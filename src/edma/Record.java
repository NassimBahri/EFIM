package edma;

import java.util.HashMap;
import java.util.Map;

/**
 * <strong>The Record class</strong> represents a row of the probability table
 * 
 * @author Nassim BAHRI
 * @version 1.0
 *
 */
public class Record {

	/**
	 * Row
	 */
	public Map<String, Double> rows;

	/**
	 * Default constructor of the Record class
	 */
	public Record() {
		this.rows = new HashMap<String, Double>();
	}

	/**
	 * Add new row
	 * 
	 * @param key
	 *            Key of the row (item)
	 * @param value
	 *            Support of the item
	 */
	public void add(String key, double value) {
		this.rows.put(key, value);
	}

	/**
	 * Get the support of an item
	 * 
	 * @param key
	 *            Item
	 * @return Support value
	 */
	public double get(String key) {
		return (this.rows.containsKey(key)) ? this.rows.get(key) : 0;
	}

}
