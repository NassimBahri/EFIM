package edma;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import utils.Collections;
import utils.Number;
import utils.FrequentItemSet;

public class EDMA {
	
	/**
	 * Support based methods
	 */
	public enum SupportBased {
		pr, bel
	}

	/**
	 * Probability table
	 */
	public ArrayList<Record> probabilityTable;
	/**
	 * Size of the database
	 */
	public int size = 0;
	/**
	 * Domains of the attributes
	 */
	private Map<String, ArrayList<String>> domains;

	/**
	 * Custom constructor of the EDMA class
	 * 
	 * @param file
	 *            Path to the data file
	 * @param sb
	 *            Support based method
	 */
	public EDMA(String file, SupportBased sb) {
		this.probabilityTable = new ArrayList<Record>();
		this.generateDomain(file);
		this.generateProbabilityTable(file, sb);
	}

	/**
	 * Create the domain of the attributes
	 * 
	 * @param filePath
	 *            Path to the data file
	 */
	@SuppressWarnings("resource")
	private void generateDomain(String filePath) {
		File file = new File(filePath);
		String elementsLine;
		String[] elementsList;
		this.domains = new HashMap<String, ArrayList<String>>();
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1;
			while ((elementsLine = buffer.readLine()) != null) {
				buffer.readLine();
				elementsList = elementsLine.split(";");
				for (int i = 0; i < elementsList.length; i++) {
					String key = elementsList[i].substring(0, 1);
					if (!this.domains.containsKey(key)) {
						this.domains.put(key, new ArrayList<String>());
					}
					String[] focalElements = elementsList[i].split(" ");
					for (String item : focalElements) {
						this.addUnique(key, item);
					}
				}
				// Add the record to the probability table
				lineNumber++;
			}
			this.size = lineNumber - 1;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the Probability table
	 * 
	 * @param filePath
	 *            Path to the data file
	 * @param sb
	 *            Support based method
	 */
	@SuppressWarnings("resource")
	private void generateProbabilityTable(String filePath, SupportBased sb) {
		File file = new File(filePath);
		String elementsLine, massLine;
		String[] elementsList, massList;
		String[] cl; // confidence level
		double support;
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1;
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(";");
				massList = massLine.split(";");
				cl = massList[massList.length - 1].split(" ");
				// Initialize the record
				Record record = new Record();
				for (int i = 0; i < elementsList.length; i++) {
					String key = elementsList[i].substring(0, 1);
					// Generate power set
					ArrayList<String> set = this.domains.get(key);
					for (String item : set) {
						if (sb == SupportBased.pr) {
							support = this.calculateSupportForPT(item, elementsList[i], massList[i]);
						} else {
							support = this.calculateBelSupportForPT(item, elementsList[i], massList[i]);
						}
						record.add(item, support * Double.parseDouble(cl[0]));
					}
				}
				// Add the record to the probability table
				this.probabilityTable.add(record);
				lineNumber++;
			}
			this.size = lineNumber - 1;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Calculate the support of an item
	 * 
	 * @param item
	 *            Focal element
	 * @param elementsLine
	 *            A normalized BBA
	 * @param massesLine
	 *            Mass of the focal elements includes in the BBA
	 * @return Support value
	 */
	private double calculateSupportForPT(String item, String elementsLine, String massesLine) {
		double support = 0;
		String[] elementsList = elementsLine.split(" "), massList = massesLine.split(" ");
		for (int i = 0; i < elementsList.length; i++) {
			String[] singletons = elementsList[i].split(",");
			int nb = Collections.calculateIntersection(item.split(","), singletons);
			double mass = (double) nb / singletons.length;
			support += mass * Double.parseDouble(massList[i]);
		}
		return Number.round(support);
	}

	/**
	 * Calculate the support of an item (belief-based support)
	 * 
	 * @param item
	 *            Focal element
	 * @param elementsLine
	 *            A normalized BBA
	 * @param massesLine
	 *            Mass of the focal elements includes in the BBA
	 * @return Support value
	 */
	private double calculateBelSupportForPT(String item, String elementsLine, String massesLine) {
		double support = 0;
		String[] items = item.split(",");
		String[] elementsList = elementsLine.split(" "), massList = massesLine.split(" ");
		for (int i = 0; i < elementsList.length; i++) {
			String[] singletons = elementsList[i].split(",");
			if (Collections.isSubsetOf(singletons, items)) {
				support += Double.parseDouble(massList[i]);
			}
		}
		return Number.round(support);
	}

	/**
	 * Generate the frequent itemsets
	 * 
	 * @param minSupp
	 *            Threshold of support
	 */
	public Map<Integer, ArrayList<FrequentItemSet>> ComputeFrequentItemsets(double minSupp) {
		Map<Integer, ArrayList<FrequentItemSet>> frequentItemsets = new HashMap<Integer, ArrayList<FrequentItemSet>>();
		int k = 1;
		// Frequent 1-itemset
		frequentItemsets.put(k, new ArrayList<FrequentItemSet>());
		for (Entry<String, ArrayList<String>> domain : this.domains.entrySet()) {
			for (String item : domain.getValue()) {
				// Check if the itemset already exists
				if (this.checkItemset(item, frequentItemsets.get(k))) {
					continue;
				}
				double support = 0;
				for (Record record : this.probabilityTable) {
					support += record.get(item);
				}
				if (Number.round(support / this.size) >= minSupp) {
					frequentItemsets.get(k).add(new FrequentItemSet(item, Number.round(support / this.size)));
				}
			}
		}
		// Frequent k-itemset
		while (frequentItemsets.get(k).size() != 0) {
			ArrayList<FrequentItemSet> freq = frequentItemsets.get(k);
			ArrayList<String> candidates = new ArrayList<String>();
			for (int i = 0; i < freq.size(); i++) {
				for (int j = i + 1; j < freq.size(); j++) {
					String generated = this.generateItemset(freq.get(i).itemset, freq.get(j).itemset, k + 1);
					if (!generated.isEmpty()) {
						candidates.add(generated);
					}
				}
			}
			k++;
			frequentItemsets.put(k, new ArrayList<FrequentItemSet>());
			for (String item : candidates) {
				// Check if the itemset already exists
				if (this.checkItemset(item, frequentItemsets.get(k))) {
					continue;
				}
				double support = this.supportEstimation(item);
				if (Number.round(support) >= minSupp) {
					frequentItemsets.get(k).add(new FrequentItemSet(item, Number.round(support)));
				}
			}
		}
		return frequentItemsets;
	}

	/**
	 * Generate a candidate itemset from two frequent itemsets
	 * 
	 * @param item1
	 *            Frequent item 1
	 * @param item2
	 *            Frequent item 2
	 * @param level
	 *            Level
	 * @return Candidate itemset
	 */
	private String generateItemset(String item1, String item2, int level) {
		String itemset = "";
		boolean equals = true;
		if (item1.substring(0, 1).equals(item2.substring(0, 1))) {
			return "";
		}
		if (level == 2) {
			itemset = item1 + ";" + item2;
		} else {
			String[] items1 = item1.split(";"), items2 = item2.split(";");
			for (int i = 0; i < items1.length - 1; i++) {
				if (items1[i].equals(items2[i])) {
					itemset += items1[i] + ";";
				} else {
					equals = false;
					break;
				}
			}
			if (equals) {
				itemset += items1[items1.length - 1] + ";" + items2[items1.length - 1];
			}
		}
		return itemset;
	}

	/**
	 * Calculate the support of an itemset
	 * 
	 * @param itemset
	 *            Itemset
	 * @return Support value
	 */
	private double supportEstimation(String itemset) {
		double support = 0;
		double supportTansaction;
		String[] items = itemset.split(";");
		for (Record record : this.probabilityTable) {
			supportTansaction = 1;
			for (Entry<String, Double> item : record.rows.entrySet()) {
				if (Collections.isSubsetOf(new String[] { item.getKey() }, items)) {
					supportTansaction *= item.getValue();
				}
			}
			support += supportTansaction;
		}
		return Number.round(support / this.size);
	}

	/**
	 * Add a value to the domain of an attribute
	 * 
	 * @param key
	 *            Attribute
	 * @param item
	 *            Item to insert
	 */
	private void addUnique(String key, String item) {
		ArrayList<String> list = this.domains.get(key);
		boolean exists = false;
		for (String element : list) {
			if (element.equals(item)) {
				exists = true;
				break;
			}
		}
		if (!exists) {
			this.domains.get(key).add(item);
		}
	}

	/**
	 * Check if the itemset already exists
	 * 
	 * @param itemset
	 *            The itemset to check
	 * @param list
	 *            Frequent itemsets
	 * @return true if the itemset exists
	 */
	private boolean checkItemset(String itemset, ArrayList<FrequentItemSet> list) {
		boolean exists = false;
		for (FrequentItemSet item : list) {
			if (item.itemset.equals(itemset)) {
				return true;
			}
		}
		return exists;
	}

}
