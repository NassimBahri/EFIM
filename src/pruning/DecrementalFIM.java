package pruning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import fimed.Cell;
import fimed.Item;
import fimed.RidLists;
import utils.FrequentItemSet;
import utils.Number;

public class DecrementalFIM {

	/**
	 * RidLists structure
	 */
	private RidLists ridLists;
	/**
	 * Keys of the RidLists
	 */
	private Object[] ridListKeys;

	/**
	 * Custom constructor of the FIMED class
	 * 
	 * @param file
	 *            Path to the data file
	 * @param minSupp
	 *            Threshold of support
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DecrementalFIM(String file) {
		this.ridLists = new RidLists(file);
		this.ridListKeys = this.ridLists.records.keySet().toArray();
		Arrays.sort(ridListKeys, new Comparator() {
			public int compare(Object o1, Object o2) {
				String[] items1 = o1.toString().split(","), items2 = o2.toString().split(",");
				if (items1.length > items2.length) {
					return -1;
				}
				return 1;
			}
		});
	}

	/**
	 * Generate the frequent itemsets from the RidLists structure
	 * 
	 * @param minSupp
	 *            Threshold of support
	 */
	public Map<Integer, ArrayList<FrequentItemSet>> ComputeFrequentItemsets(double minSupp) {
		Map<Integer, ArrayList<FrequentItemSet>> frequentItemsets = new HashMap<Integer, ArrayList<FrequentItemSet>>();
		ArrayList<String> os = new ArrayList<>();
		int k = 1;
		// Frequent 1-itemset
		frequentItemsets.put(k, new ArrayList<FrequentItemSet>());
		for (Object key : this.ridListKeys) {
			String _key = key.toString();
			if (this.subsetOfInfrequent(_key, os)) {
				continue;
			}
			double bel = this.calculateSupport(key.toString(), 0, minSupp);
			if (Number.round(bel) >= minSupp) {
				frequentItemsets.get(k).add(new FrequentItemSet(_key, Number.round(bel)));
			} else {
				// Infrequent itemset
				os.add(_key);
			}
		}
		// Frequent k-itemset
		while (frequentItemsets.get(k).size() != 0) {
			ArrayList<FrequentItemSet> freq = frequentItemsets.get(k);
			ArrayList<String> candidates = new ArrayList<String>();
			for (int i = 0; i < freq.size(); i++) {
				for (int j = i + 1; j < freq.size(); j++) {
					String generated = this.generateItemset(freq.get(i).itemset, freq.get(j).itemset, k + 1);
					if (!generated.isEmpty()) {
						candidates.add(generated);
					}
				}
			}
			k++;
			frequentItemsets.put(k, new ArrayList<FrequentItemSet>());
			for (String item : candidates) {
				if (this.subsetOfInfrequent(item, os)) {
					continue;
				}
				double bel = this.calculateSupport(item, this.getItemsetSupport(frequentItemsets.get(k - 1), item, k),
						minSupp);
				if (Number.round(bel) >= minSupp) {
					frequentItemsets.get(k).add(new FrequentItemSet(item, Number.round(bel)));
				} else {
					os.add(item);
				}
			}
		}
		return frequentItemsets;
	}

	/**
	 * Verify if an itemset is a subset of an infrequent itemset
	 * 
	 * @param itemset
	 *            Itemset to verify
	 * @param os
	 *            Collection of infrequent itemsets
	 * @return True if the itemset is a subset of an infrequent itemset
	 */
	private boolean subsetOfInfrequent(String itemset, ArrayList<String> os) {
		for (String element : os) {
			if (utils.Collections.isSubsetOf(itemset.split(","), element.split(","))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Calculate the support of an itemset
	 * 
	 * @param itemset
	 *            Itemset to calculate its belief
	 * @return Ssupport value
	 */
	private double calculateSupport(String itemset, double initialBel, double threshold) {
		double bel = 0;
		Cell temp;
		String[] items = itemset.split(";");
		initialBel *= this.ridLists.size;
		// if 1-itemset
		if (items.length == 1) {
			temp = this.ridLists.get(itemset);
			for (Item item : temp.items) {
				bel += item.belief;
			}
			return bel / this.ridLists.size;
		} else {
			// K-itemsets
			temp = this.ridLists.get(items[0]);
			int size = items.length;
			ArrayList<Item> collection = new ArrayList<Item>();
			for (Item item : temp.items) {
				collection.add(item);
				for (int i = 1; i < size - 1; i++) {
					Item search = this.getItem(items[i], item.rowid);
					if (search == null) {
						break;
					}
					collection.add(search);
				}
				if (collection.size() < items.length - 1) {
					collection.clear();
					continue;
				}
				// Get the last item
				Item last = this.getItem(items[size - 1], item.rowid);
				if (last == null) {
					last = new Item(item.rowid, 0, 0, 0);
				}
				double over = 1;
				for (Item itm : collection) {
					over *= itm.belief;
				}
				initialBel -= (over * (1 - last.belief));
				if (initialBel / this.ridLists.size < threshold) {
					return initialBel / this.ridLists.size;
				}
				collection.clear();
			}
		}
		return initialBel / this.ridLists.size;
	}

	/**
	 * Get an item from the rids
	 * 
	 * @param itemset
	 *            Label of the itemset
	 * @param rowid
	 *            Record identifier
	 * @return Item
	 */
	private Item getItem(String itemset, int rowid) {
		Cell cell = this.ridLists.records.get(itemset);
		for (Item item : cell.items) {
			if (item.rowid == rowid) {
				return item;
			}
		}
		return null;
	}

	/**
	 * Generate a candidate itemset from two frequent itemsets
	 * 
	 * @param item1
	 *            Frequent item 1
	 * @param item2
	 *            Frequent item 2
	 * @param level
	 *            Level
	 * @return Candidate itemset
	 */
	private String generateItemset(String item1, String item2, int level) {
		String itemset = "";
		boolean equals = true;
		if (item1.substring(0, 1).equals(item2.substring(0, 1))) {
			return "";
		}
		if (level == 2) {
			itemset = item1 + ";" + item2;
		} else {
			String[] items1 = item1.split(";"), items2 = item2.split(";");
			for (int i = 0; i < items1.length - 1; i++) {
				if (items1[i].equals(items2[i])) {
					itemset += items1[i] + ";";
				} else {
					equals = false;
					break;
				}
			}
			if (equals) {
				itemset += items1[items1.length - 1] + ";" + items2[items1.length - 1];
			}
		}
		return itemset;
	}

	/**
	 * Get the support of frequent itemset
	 * 
	 * @param frequents
	 *            Collection of frquent itemsets
	 * @param itemset
	 *            Itemset to get its support
	 * @return Support value
	 */
	private double getItemsetSupport(ArrayList<FrequentItemSet> frequents, String itemset, int level) {
		String[] items = itemset.split(";");
		String label = "";
		for (int i = 0; i < level - 1; i++) {
			label += items[i];
			if (i < level - 2) {
				label += ";";
			}
		}
		for (FrequentItemSet f : frequents) {
			if (f.itemset.equals(label)) {
				return f.support;
			}
		}
		return 0;
	}

}
