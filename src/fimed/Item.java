package fimed;

/**
 * <strong>The Item class</strong> stores for each focal element its row
 * identifier, its mass and its belief value.
 * 
 * @author Nassim BAHRI
 * @version 1.0
 *
 */
public class Item {

	/**
	 * Row identifier of the focal element
	 */
	public int rowid;
	/**
	 * Mass value of the focal element
	 */
	public double mass;
	/**
	 * Belief value of the focal element
	 */
	public double belief;
	/**
	 * Plausibility of the focal element
	 */
	public double plausibility;

	/**
	 * Default constructor of the class Item
	 */
	public Item() {
	}

	/**
	 * Custom constructor of the class Item
	 * 
	 * @param rowid
	 *            Row identifier
	 * @param mass
	 *            Mass value
	 * @param bel
	 *            Belief value
	 * @param pl
	 *            Plausibility value
	 */
	public Item(int rowid, double mass, double bel, double pl) {
		this.rowid = rowid;
		this.mass = mass;
		this.belief = bel;
		this.plausibility = pl;
	}

	/**
	 * Convert object to string
	 * 
	 * @return Item details
	 */
	public String toString() {
		// return "(" + this.rowid + "," + this.mass + "," + this.belief + "," +
		// this.plausibility + ")";
		return "(" + this.rowid + "," + this.belief + ")";

	}

}
