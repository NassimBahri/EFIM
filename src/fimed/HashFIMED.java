package fimed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import utils.FrequentItemSet;

/**
 * <strong>The FIMED class</strong> allows discovering frequent itemsets in an
 * evidential database.
 * 
 * @author Nassim BAHRI
 * @version 1.0
 *
 */
public class HashFIMED {

	/**
	 * RidLists structure
	 */
	private RidLists ridLists;

	public static void main(String[] args) {
		long startTime = System.currentTimeMillis(); // start time
		new HashFIMED("datasets/data.data", 0.9);
		long stopTime = System.currentTimeMillis(); // end time
		long elapsedTime = stopTime - startTime; // execution time
		System.out.println("Delay (basic structure):  " + elapsedTime);
	}

	/**
	 * Custom constructor of the FIMED class
	 * 
	 * @param file
	 *            Path to the data file
	 * @param minSupp
	 *            Threshold of support
	 */
	public HashFIMED(String file, double minSupp) {
		this.ridLists = new RidLists(file);
		/*
		 * for(Entry<String, Cell> c : this.ridLists.records.entrySet()) {
		 * System.out.println(c.getKey()+" : "+c.getValue()); }
		 * System.out.println("-------");
		 */
		this.ComputeFrequentItemsets(minSupp);
	}

	/**
	 * Calculate the belief of an itemset
	 * 
	 * @param itemset
	 *            Itemset to calculate its belief
	 * @return Belief value
	 */
	private double Bel(String itemset) {
		double bel = 0;
		Cell temp;
		String[] items = itemset.split(";");
		// if 1-itemset
		if (items.length == 1) {
			temp = this.ridLists.records.get(itemset);
		} else {
			temp = this.ridLists.records.get(items[0]);
			for (int i = 1; i < items.length; i++) {
				temp = this.recursiveBel(temp, this.ridLists.records.get(items[i]));
			}
		}
		for (Item item : temp.items) {
			bel += item.belief;
		}
		return bel / this.ridLists.size;
		// return new BigDecimal(bel / this.ridLists.size).setScale(2,
		// BigDecimal.ROUND_HALF_EVEN).doubleValue();
	}

	/**
	 * Calculate the intersection between two cells
	 * 
	 * @param cell1
	 *            Cell 1: rids of the first itemset
	 * @param cell2
	 *            Cell 2: rids of the second itemset
	 * @return List of items representing the intersection
	 */
	private Cell recursiveBel(Cell cell1, Cell cell2) {
		Cell temp = new Cell();
		for (Item item1 : cell1.items) {
			for (Item item2 : cell2.items) {
				if (item1.rowid == item2.rowid) {
					temp.items.add(new Item(item1.rowid, item1.mass * item2.mass, item1.belief * item2.belief,
							item1.plausibility * item2.plausibility));
				}
			}
		}
		return temp;
	}

	/**
	 * Generate the frequent itemsets from the RidLists structure
	 * 
	 * @param minSupp
	 *            Threshold of support
	 */
	private void ComputeFrequentItemsets(double minSupp) {
		Map<Integer, ArrayList<FrequentItemSet>> frequentItemsets = new HashMap<Integer, ArrayList<FrequentItemSet>>();
		int k = 1;
		// Frequent 1-itemset
		frequentItemsets.put(k, new ArrayList<FrequentItemSet>());
		for (Entry<String, Cell> cell : this.ridLists.records.entrySet()) {
			double bel = this.Bel(cell.getKey());
			if (bel >= minSupp) {
				frequentItemsets.get(k).add(new FrequentItemSet(cell.getKey(), bel));
			}
		}
		// Frequent k-itemset
		while (frequentItemsets.get(k).size() != 0) {
			ArrayList<FrequentItemSet> freq = frequentItemsets.get(k);
			ArrayList<String> candidates = new ArrayList<String>();
			for (int i = 0; i < freq.size(); i++) {
				for (int j = i + 1; j < freq.size(); j++) {
					String generated = this.generateItemset(freq.get(i).itemset, freq.get(j).itemset, k + 1);
					if (!generated.isEmpty()) {
						candidates.add(generated);
					}
				}
			}
			k++;
			frequentItemsets.put(k, new ArrayList<FrequentItemSet>());
			for (String item : candidates) {
				double bel = this.Bel(item);
				// System.out.println("Candidate is : " + item + " bel is " + bel);
				if (bel >= minSupp) {
					frequentItemsets.get(k).add(new FrequentItemSet(item, bel));
				}
			}
		}
		int nb = 0;
		for (Entry<Integer, ArrayList<FrequentItemSet>> item : frequentItemsets.entrySet()) {
			System.out.println("\n======= Frequent " + item.getKey() + "-itemsets =======");
			for (FrequentItemSet elem : item.getValue()) {
				System.out.println(elem.itemset + " : " + elem.support);
			}
			nb += item.getValue().size();
		}
		System.out.println("Number of frequent itemsets = " + nb);
	}

	/**
	 * Generate a candidate itemset from two frequent itemsets
	 * 
	 * @param item1
	 *            Frequent item 1
	 * @param item2
	 *            Frequent item 2
	 * @param level
	 *            Level
	 * @return Candidate itemset
	 */
	private String generateItemset(String item1, String item2, int level) {
		String itemset = "";
		boolean equals = true;
		if (item1.substring(0, 1).equals(item2.substring(0, 1))) {
			return "";
		}
		if (level == 2) {
			itemset = item1 + ";" + item2;
		} else {
			String[] items1 = item1.split(";"), items2 = item2.split(";");
			for (int i = 0; i < items1.length - 1; i++) {
				if (items1[i].equals(items2[i])) {
					itemset += items1[i] + ";";
				} else {
					equals = false;
					break;
				}
			}
			if (equals) {
				itemset += items1[items1.length - 1] + ";" + items2[items1.length - 1];
			}
		}
		return itemset;
	}

}
