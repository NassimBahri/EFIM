package fimed;

import java.util.ArrayList;

/**
 * <strong>The Cell class</strong> stores a list of couples for each entry.
 * 
 * @author Nassim BAHRI
 * @version 1.0
 *
 */
public class Cell {

	/**
	 * The list of items included in a cell
	 */
	public ArrayList<Item> items;

	/**
	 * Default constructor of the class Cell
	 */
	public Cell() {
		this.items = new ArrayList<Item>();
	}

	/**
	 * Concert an object to string
	 * 
	 * @return The list of items
	 */
	public String toString() {
		return items.toString();
	}

}
