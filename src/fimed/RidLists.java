package fimed;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import utils.Collections;
import utils.Number;

/**
 * <strong>The RidLists class</strong> is a vertical representation of the
 * evidential database.
 * 
 * @author Nassim BAHRI
 * @version 1.0
 *
 */
public class RidLists {

	/**
	 * The RidLists records
	 */
	public Map<String, Cell> records;
	/**
	 * Size of the database
	 */
	public int size = 0;

	/**
	 * Default constructor of the RidLists class
	 * 
	 * @param file
	 *            Path to the data file
	 */
	public RidLists(String file) {
		this.records = new HashMap<String, Cell>();
		this.ridListsConstruction(file);
		this.updateBeliefRidLists();
	}

	@SuppressWarnings("resource")
	/**
	 * Create the RidLists structure
	 * 
	 * @param filePath
	 *            Path to the data file
	 */
	private void ridListsConstruction(String filePath) {
		File file = new File(filePath);
		String elementsLine, massLine;
		String[] elementsList, massList;
		String[] cl; // confidence level
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1;
			while ((elementsLine = buffer.readLine()) != null) {
				massLine = buffer.readLine();
				elementsList = elementsLine.split(";");
				massList = massLine.split(";");
				cl = massList[massList.length - 1].split(" ");
				for (int i = 0; i < elementsList.length; i++) {
					String[] focalElements = elementsList[i].split(" ");
					String[] focalElementsMasses = massList[i].split(" ");
					for (int j = 0; j < focalElements.length; j++) {
						this.add(focalElements[j],
								new Item(lineNumber, Double.parseDouble(focalElementsMasses[j]),
										Number.round(Double.parseDouble(focalElementsMasses[j]) * Double.parseDouble(cl[0])),
										Number.round(Double.parseDouble(focalElementsMasses[j]) * Double.parseDouble(cl[1]))));
					}
				}
				lineNumber++;
			}
			this.size = lineNumber - 1;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	/**
	 * Update the RidLists structure
	 */
	private void updateBeliefRidLists() {
		// sort the rid list in a decreasing order following to the sizes of the items
		// to avoid multiple updates of the same item.
		Object[] ridListKeys = this.records.keySet().toArray();
		Arrays.sort(ridListKeys, new Comparator() {
			public int compare(Object o1, Object o2) {
				String[] items1 = o1.toString().split(","), items2 = o2.toString().split(",");
				if (items1.length > items2.length) {
					return -1;
				}
				if (items1.length < items2.length) {
					return 1;
				}
				return 0;
			}
		});
		for (Object key : ridListKeys) {
			Cell cell = this.records.get(key.toString());
			String[] focalElements = key.toString().split(",");
			// if singleton
			if (focalElements.length == 1) {
				continue;
			}
			for (Entry<String, Cell> cell2 : this.records.entrySet()) {
				String[] focalElements2 = cell2.getKey().split(",");
				if (focalElements.length <= focalElements2.length) {
					continue;
				}
				if (Collections.isSubsetOf(focalElements2, focalElements)) {
					//System.out.println(cell2.getKey()+" subset of "+key);
					//System.out.println(cell);
					//System.out.println(cell2.getValue());
					//System.out.println("--------------");
					ArrayList<Item> itemsCell1 = cell.items, itemsCell2 = cell2.getValue().items;
					for (int i = 0; i < itemsCell2.size(); i++) {
						int j = 0;
						boolean itemExist = false;
						while (j < itemsCell1.size() && !itemExist) {
							if (itemsCell2.get(i).rowid == itemsCell1.get(j).rowid) {
								itemsCell1.get(j).belief = itemsCell1.get(j).belief + itemsCell2.get(i).belief;
								itemExist = true;
							}
							j++;
						}
						if (!itemExist) {
							itemsCell1.add(new Item(itemsCell2.get(i).rowid, itemsCell2.get(i).mass,
									itemsCell2.get(i).belief, itemsCell2.get(i).plausibility));
						}
					}
				}
			}
		}
	}

	/**
	 * Add new item to the RidLists
	 * 
	 * @param key
	 *            Focal element
	 * @param item
	 *            Item to insert
	 */
	private void add(String key, Item item) {
		if (!this.records.containsKey(key)) {
			this.records.put(key, new Cell());
		}
		this.records.get(key).items.add(item);
	}

	/**
	 * Convert an object to string
	 * 
	 * @return The RisLists
	 */
	public String toString() {
		String result = "";
		for (Entry<String, Cell> cell : this.records.entrySet()) {
			result += cell.getKey() + " : ";
			result += cell.getValue();
			result += "\n";
		}
		return result;
	}

	public Cell get(String key) {
		for (Entry<String, Cell> cell : this.records.entrySet()) {
			if (cell.getKey().equals(key)) {
				return cell.getValue();
			}
		}
		return null;
	}

}
