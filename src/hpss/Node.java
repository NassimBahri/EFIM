package hpss;

import java.util.ArrayList;

/**
 * <strong>The Node class</strong> represents a node of in a tree data
 * structure. It has a label, a mass value corresponding to the specified label
 * and a list of children.
 * 
 * @author Nassim BAHRI
 * @version 1.0
 *
 */
public class Node {

	/**
	 * Focal element
	 */
	public String focalElement;
	/**
	 * The mass
	 */
	public double mass;
	/**
	 * The node's children
	 */
	public ArrayList<Node> children;

	/**
	 * Default constructor of the class Node
	 */
	public Node() {
	}

	/**
	 * Custom constructor for the class Node
	 * 
	 * @param focalElement
	 *            Focal element
	 * @param mass
	 *            Mass of the focal element
	 */
	public Node(String focalElement, double mass) {
		this.focalElement = focalElement;
		this.mass = mass;
		this.children = new ArrayList<Node>();
	}

	/**
	 * Search for a node
	 * 
	 * @param value
	 *            The label of the node looking for
	 * @return Node object having the specified label
	 */
	public Node getNode(String value) {
		Node result = null;
		if (this.focalElement.equals(value)) {
			return this;
		} else {
			for (Node child : this.children) {
				result = child.getNode(value);
				if (result != null && result.focalElement.equals(value)) {
					return result;
				}
			}
			return result;
		}
	}

	/**
	 * Add new child to the current node
	 * 
	 * @param node
	 *            Node object to insert
	 */
	public void addChild(Node node) {
		boolean exist = false;
		for (Node child : this.children) {
			if (child.focalElement.equals(node.focalElement)) {
				child.mass += node.mass;
				exist = true;
				break;
			}
		}
		if (!exist) {
			this.children.add(node);
		}
	}

	/**
	 * Convert object to String
	 */
	public String toString() {
		String ch = "";
		ch += "(" + this.focalElement + " , " + this.mass + ")\n";
		if (this.children.size() > 0) {
			for (Node child : this.children) {
				ch += "|-" + child;
			}
		}
		return ch;
	}

}