package hpss;

import java.util.ArrayList;

import utils.Collections;

public class HPSS {

	/**
	 * Bit data structure
	 */
	private Bit bit;

	/*
	 * public static void main(String[] args) { new HPSS("datasets/article.data",
	 * 0.5); }
	 */

	/**
	 * Custom constructor of the HPSS class
	 * 
	 * @param file
	 *            Path to the data file
	 * @param minSupp
	 *            Threshold f support
	 */
	public HPSS(String file, double minSupp) {
		this.bit = new Bit(file);
		// ArrayList<ArrayList<String>> paths = new ArrayList<ArrayList<String>>();
		// paths = this.generateMax(this.bit.root, paths, new ArrayList<String>(),
		// minSupp);
		// System.out.println(paths);
		// ArrayList<String> frequents = this.generateFim(paths);
		// System.out.println("---------------");
		// System.out.println(frequents);
	}

	/**
	 * Generation of frequent itemsets from the BIT
	 * 
	 * @param selectedNode
	 *            Root of the subtree
	 * @param paths
	 *            Possible paths representing the frequent itemsets
	 * @param previous
	 *            Path from the root to the current node
	 * @param minSupp
	 *            Threshold of support
	 * @return List of frequent itmesets
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	private ArrayList<ArrayList<String>> generateMax(Node selectedNode, ArrayList<ArrayList<String>> paths,
			ArrayList<String> previous, double minSupp) {
		// System.out.println("previous : " + previous);
		if (selectedNode.mass / this.bit.size < minSupp) {
			// paths.add(previous);
			return paths;
		}
		if (selectedNode.children.size() == 0) {
			previous.add(selectedNode.focalElement);
			paths.add(previous);
			return paths;
		}
		for (Node node : selectedNode.children) {
			// System.out.println(node.focalElement);
			ArrayList<String> temp = (ArrayList<String>) previous.clone();
			temp.add(selectedNode.focalElement);
			this.generateMax(node, paths, temp, minSupp);
		}
		return paths;
	}

	/**
	 * Generate frequent itemsets
	 * 
	 * @param itemsets
	 *            List of items
	 * @return Frequent itemsets
	 */
	@SuppressWarnings("unused")
	private ArrayList<String> generateFim(ArrayList<ArrayList<String>> itemsets) {
		ArrayList<String> frequents = new ArrayList<String>();
		for (ArrayList<String> item : itemsets) {
			System.out.println(item);
			ArrayList<String> result = Collections.generatePowerSet(item);
			for (String element : result) {
				if (!frequents.contains(element)) {
					frequents.add(element);
				}
			}
		}
		return frequents;
	}

}
