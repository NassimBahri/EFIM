package hpss;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class Bit {

	/**
	 * Root node of the tree
	 */
	public Node root;
	/**
	 * Size of the database
	 */
	public int size = 0;

	public static void main(String[] args) {
		new Bit("datasets/article.data");
	}

	/**
	 * Default constructor of the Bit class
	 * 
	 * @param filePath
	 *            Path to the data file
	 */
	@SuppressWarnings({ "resource", "unused" })
	public Bit(String filePath) {
		this.root = new Node("∅", 1);
		File file = new File(filePath);
		String elementsLine, massLine;
		String[] elementsList, massList;
		String[] cl; // confidence level
		int attribute, previousAttribute = 1;
		Node child;
		HashMap<Integer, ArrayList<String>> previous;
		ArrayList<ArrayList<String>> possiblePaths = new ArrayList<ArrayList<String>>();
		try {
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader buffer = new BufferedReader(reader);
			int lineNumber = 1;
			while ((elementsLine = buffer.readLine()) != null) {
				child = this.root;
				massLine = buffer.readLine();
				elementsList = elementsLine.split(";");
				massList = massLine.split(";");
				cl = massList[massList.length - 1].split(" ");
				attribute = 1;
				previousAttribute = 1; // Position of the previous attribute in the dataset
				previous = new HashMap<Integer, ArrayList<String>>();
				for (int i = 0; i < elementsList.length; i++) {
					String[] focalElements = elementsList[i].split(" ");
					String[] focalElementsMasses = massList[i].split(" ");
					for (int j = 0; j < focalElements.length; j++) {
						// Calculate the belief of the item
						double mass = Double.parseDouble(focalElementsMasses[j]) * Double.parseDouble(cl[0]);
						// double belief = this.calculateBel(focalElements[j], focalElements,
						// focalElementsMasses,Double.parseDouble(cl[0]));
						// Insert attribute's values at the first level
						if (attribute == 1) {
							child.addChild(new Node(focalElements[j], mass));
						} else {
							// Generating all possible paths
							if (previousAttribute != attribute) {
								possiblePaths = new ArrayList<ArrayList<String>>();
								possiblePaths = calculatePaths(possiblePaths, previous, 1);
								previousAttribute = attribute;
							}
							// Insert the nodes
							for (ArrayList<String> path : possiblePaths) {
								child = this.root;
								for (String element : path) {
									child = child.getNode(element);
								}
								// Update the leaf nodes
								double newMass = mass;
								for (Node node : child.children) {
									System.out.println(node.focalElement + " VS " + focalElements[j]);
									if (node.focalElement.equals(focalElements[j])) {
										continue;
									} else if (utils.Collections.isSubsetOf(focalElements[j].split(","),
											node.focalElement.split(","))) {
										node.mass += mass;
									} else if (utils.Collections.isSubsetOf(node.focalElement.split(","),
											focalElements[j].split(","))) {
										newMass += node.mass;
									}
								}
								child.addChild(new Node(focalElements[j], newMass));
							}
						}
						if (previous.get(attribute) == null) {
							previous.put(attribute, new ArrayList<String>());
						}
						previous.get(attribute).add(focalElements[j]);
					}
					attribute++;
				}
				lineNumber++;
			}
			this.size = lineNumber - 1;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generating all possible paths (Cartesian product)
	 * 
	 * @param possiblePaths
	 *            All possible paths from the items that belong to the previous
	 *            attributes
	 * @param focalElements
	 *            Previous focal element (corresponding to the previous attributes)
	 * @param startAttribute
	 *            Position of the attribute being treated
	 * @return All possible paths to insert a focal element
	 */
	@SuppressWarnings("serial")
	private ArrayList<ArrayList<String>> calculatePaths(ArrayList<ArrayList<String>> possiblePaths,
			HashMap<Integer, ArrayList<String>> focalElements, int startAttribute) {
		if (possiblePaths.size() == 0) {
			ArrayList<String> list1 = focalElements.get(startAttribute);
			ArrayList<String> list2 = focalElements.get(startAttribute + 1);
			if (list2 != null) {
				for (String item1 : list1) {
					ArrayList<String> line = new ArrayList<String>();
					line.add(item1);
					possiblePaths.add(line);
					for (String item2 : list2) {
						line.add(item2);
					}
				}
			} else {
				for (String item : list1) {
					possiblePaths.add(new ArrayList<String>() {
						{
							add(item);
						}
					});
				}
			}
		} else {
			ArrayList<ArrayList<String>> temp = new ArrayList<ArrayList<String>>();
			ArrayList<String> list2 = focalElements.get(startAttribute + 1);
			for (ArrayList<String> list1 : possiblePaths) {
				for (String item2 : list2) {
					ArrayList<String> line = new ArrayList<String>();
					temp.add(line);
					line.addAll(list1);
					line.add(item2);
				}
			}
			possiblePaths = temp;
		}
		if (startAttribute + 1 < focalElements.size()) {
			return this.calculatePaths(possiblePaths, focalElements, startAttribute + 1);
		}
		return possiblePaths;
	}

}
